export const DIAMETER = 12103.6;

export const VOLUME = {
  CARBON_DIOXIDE: 96.5,
  HYDROGEN: 0,
  HELIUM: 10,
  NITROGEN: 3.5,
};
