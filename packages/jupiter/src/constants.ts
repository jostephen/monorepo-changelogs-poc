export const DIAMETER = 142984;

export const VOLUME = {
  CARBON_DIOXIDE: 0,
  HYDROGEN: 90,
  HELIUM: 10,
  NITROGEN: 0,
};
